<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'gourmetentre' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '70}==6-[4>beAKRi[/r*H|N_Y71GG/Kjwy05@jf6J2WE?r`n9tH?8NJ9D|%79nf7' );
define( 'SECURE_AUTH_KEY',  '28S?ic!&H9N:a0*}]evX+/K)cn|F|Fv)4|/:~`yGU/~u>|^xH.?ErF5-N_2Wd]oN' );
define( 'LOGGED_IN_KEY',    'ImNXnb8:(OY.ZjXi.+;;$638e?jR~S6+W:Zk@>Y/{e#}I*9:9>hu-yvgDev2h(d;' );
define( 'NONCE_KEY',        'g0qXSF3.~&^XdOATbdBd>vb|qr:6ANUWPTLY4(3+b imbmA04je<V/e3By>r]+ou' );
define( 'AUTH_SALT',        '>ubg?YZ?:{X.&~ib!40i}Bm,_)?}UkT S0*@IAD0qe(Fr_cB&z7U,nXiG*afaoLY' );
define( 'SECURE_AUTH_SALT', 'J7i-o/s2[,|.j[VO_u3x|^SiRi.[s=tUZ8xbV+2@m<9^BU.:a0C~]p9n}bNH~CPQ' );
define( 'LOGGED_IN_SALT',   '8WpYO91bQi1=Y+/q]2[x4RIe=b0Vx@u[KMhZn5AyIV}07TbCM{^dy>Wk!hNt3F*t' );
define( 'NONCE_SALT',       '4T]ZWZ(RL--Z1L*t<,4Z0q)9uqPojLeYy.kuwBokD^:i[TUrepO*NOXb94At?s22' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
