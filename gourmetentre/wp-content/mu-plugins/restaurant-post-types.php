<?php 
function restaurant_post_types(){
       
        register_post_type('Recipe',array(
            'capability_type' => 'recipe',
            'map_meta_cap'=> true,
            'supports' => array('title', 'editor','thumbnail'),
            'rewrite'=> array('slug' => 'recipe' ),
            'has_archive' => true,
            'public' => true, 
            'labels' => array(
                'name' => "Recipe",
                'add_new_item' => 'Add New Recipe',
                'edit_item' => 'Edit Recipe',
                'all_items' => 'All Recipes',
                'singular_name' => "Recipe"
            ),
            'menu_icon' => 'dashicons-carrot'
        ));
        register_post_type('Chefs',array(
            'capability_type' => 'chef',
            'map_meta_cap'=> true,
            'supports' => array('title', 'editor','thumbnail'),
            'public' => true, 
            'labels' => array(
                'name' => "Chefs",
                'add_new_item' => 'Add New Chef',
                'edit_item' => 'Edit Chef',
                'all_items' => 'All Chefs',
                'singular_name' => "Chef"
            ),
            'menu_icon' => 'dashicons-index-card'
        ));
}
 add_action('init', 'restaurant_post_types');

function create_posttype() {
  $args = array(
      'labels'=> array(
      'name' => 'Testimonials',
      'singular_name' => 'Testimonials',
      'all_items' => 'All Testimonials',
      'add_new_item' => 'Add New Testimonial',
      'edit_item' => 'Edit Testimonial',
      'view_item' => 'View Testimonial'
    ),
    'public' => true,
    'has_archive' => true,
    'rewrite' => array('slug', 'testimonials'),
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_nav_menus' => true,
    'capability_type' => 'page',
    'supports' => array('title', 'editor', 'thumbnail'),
    'exclude_from_search' => true,
    'menu_position' => 80,
    'has_archive' => true,
    'menu_icon' => 'dashicons-format-status'
    );
  register_post_type('testimonials', $args);
}
add_action( 'init', 'create_posttype');

?>