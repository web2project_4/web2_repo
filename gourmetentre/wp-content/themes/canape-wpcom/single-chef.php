<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Canape
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class ="TextWrap">
				<?php the_post_thumbnail(); ?>
			</div>
				<?php the_content(); ?>
            
		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>
