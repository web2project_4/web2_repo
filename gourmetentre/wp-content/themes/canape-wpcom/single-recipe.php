<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Canape
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php while ( have_posts() ) : the_post(); ?>



			<?php get_template_part( 'template-parts/content', 'single' ); ?>

			<?php the_post_navigation(); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>
			<?php
				$relatedChefs = get_field('related_chefs');
				echo '<h2 class="header-text">Recipe Created By: </h2>';

				foreach($relatedChefs as $chefs){ //for each a post object
					?>
 					<li><a href="<?php echo get_the_permalink($chefs);?>">
					 <?php echo get_the_title($chefs);?>
						 </a>
						 </li>
				<?php
					 }
			?>

		<?php endwhile; // End of the loop. ?>


		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>
