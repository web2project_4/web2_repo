<?php



get_header(); ?>

<div class="content-wrapper full-width <?php echo esc_attr( canape_additional_class() ); ?>">
		<div id="primary" class="content-area testimonials-content-area">
			<div id="main" class="site-main testimonials grid" role="main">
				<div id="testimonials">
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>

							<?php the_posts_navigation(); ?>
					
				</div><!-- .testimonials .grid -->

			</div><!-- #content -->
		</div><!-- #primary -->
	</div>

<?php get_footer(); ?>