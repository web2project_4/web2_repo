
class Search{
    // init objects
    constructor(){
        //leveraging JQuery libray
        this.openButton = $(".js-search-trigger");
        this.closeButton = $(".js-search-overlay__close");
        this.searchOverlay = $(".search-overlay");
        this.events();
        this.isOverlayOpen= false;
    }
    

    //events
    events(){
        this.openButton.on("click", this.openOverlay.bind(this));
        this.closeButton.on("click", this.closeOverlay.bind(this));
        $(document).on("keydown", this.keyPressDispatcher.bind(this));
    }
    //open overlay
    openOverlay(){
        this.searchOverlay.addClass("search-overlay--active");
        $("body").addClass("body-no-scroll");
    }
    //close overlay
    closeOverlay(){
        this.searchOverlay.removeClass("search-overlay--active");
        $("body").removeClass("body-no-scroll");
    }

    keyPressDispatcher(e){
         console.log("this is a test");
         if(e.keyCode == 83 && !this.isOverlayOpen){
         this.openOverlay();
         }

         if(e.keyCode == 27 && this.isOverlayOpen){
            this.closeOverlay();
         }
    }
}